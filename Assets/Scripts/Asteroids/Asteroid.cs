using UnityEngine;

namespace AsteroidClicker.Asteroids
{
	public class Asteroid : MonoBehaviour
	{
		private Renderer asteroidRenderer;
		private MaterialPropertyBlock propertyBlock;
		private int lives;
		private float lifeSpan;

		private void OnEnable()
		{
			propertyBlock = new MaterialPropertyBlock();
			asteroidRenderer = GetComponent<Renderer>();
			asteroidRenderer.GetPropertyBlock(propertyBlock);
			lifeSpan = GameSettings.instance.AsteroidRandomLifeSpan;
			lives = GameSettings.instance.AsteroidLives;
			RefreshColor();

			EventBroker.OnGameReset += OnGameReset;
		}

		private void OnDisable()
		{
			EventBroker.OnGameReset -= OnGameReset;
		}

		private void Update()
		{
			ReduceLifeSpan();
		}

		private void ReduceLifeSpan()
		{
			lifeSpan -= Time.deltaTime;
			if (lifeSpan < 0)
			{
				DestroyAsteroid(false);
			}
		}

		private void RefreshColor()
		{
			int colorIndex = Mathf.Clamp(lives - 1, 0, GameSettings.instance.AsteroidColors.Length - 1);
			Color color = GameSettings.instance.AsteroidColors[colorIndex];
			propertyBlock.SetColor("_Color", color);
			asteroidRenderer.SetPropertyBlock(propertyBlock);
		}

		public void Hit()
		{
			lives--;

			if (lives <= 0)
			{
				DestroyAsteroid(true);
			}
			else
			{
				RefreshColor();
			}
		}

		private void DestroyAsteroid(bool byPlayer)
		{
			EventBroker.CallOnAsteroidDestroyed(byPlayer);
			Destroy(gameObject);
		}

		private void OnGameReset()
		{
			DestroyAsteroid(false);
		}
	}
}