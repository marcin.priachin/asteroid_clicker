using System.Collections.Generic;
using UnityEngine;

namespace AsteroidClicker.Asteroids
{
    public class AsteroidsSpawner : MonoBehaviour
    {
        private int asteroidsCount = 0;
        private Collider spawnCollider;
        private float spawnTimer;

        private void OnEnable()
        {
            spawnCollider = GetComponent<Collider>();
            spawnTimer = GameSettings.instance.AsteroidRabdomSpawnTime;
            EventBroker.OnAsteroidDestroyed += OnAsteroidDestroyed;
        }

        private void OnDisable()
        {
            EventBroker.OnAsteroidDestroyed -= OnAsteroidDestroyed;
        }

        private void Update()
        {
            if (asteroidsCount < GameSettings.instance.MaxAsteroidsCount)
            {
                spawnTimer -= Time.deltaTime;
            }
            if (asteroidsCount < GameSettings.instance.MinAsteroidsCount || spawnTimer < 0)
            {
                SpanAsteroid();
            }
        }

        private void SpanAsteroid()
        {
            spawnTimer = GameSettings.instance.AsteroidRabdomSpawnTime;
            Instantiate(GameSettings.instance.AsteroidPrefab, spawnCollider.bounds.RandomPoint(), Random.rotation);
            asteroidsCount++;
        }

        private void OnAsteroidDestroyed(bool byPlayer)
        {
            asteroidsCount--;
        }
    }
}