using System;

namespace AsteroidClicker
{ 
    public static class EventBroker
    {
        public static event Action<bool> OnAsteroidDestroyed;
        public static void CallOnAsteroidDestroyed(bool byPlayer)
        {
            OnAsteroidDestroyed?.Invoke(byPlayer);
        }

        public static event Action<int> OnScoreUpdated;
        public static void CallOnScoreUpdated(int score)
        {
            OnScoreUpdated?.Invoke(score);
        }

        public static event Action OnGameFinished;
        public static void CallOnGameFinished()
        {
            OnGameFinished?.Invoke();
        }

        public static event Action OnGameReset;
        public static void CallOnGameReset()
        {
            OnGameReset?.Invoke();
        }
    }
}
