using UnityEngine;

namespace AsteroidClicker
{
    public class GameManager : MonoBehaviour
    {
        private int score;

        private void OnEnable()
        {
            EventBroker.OnAsteroidDestroyed += OnAsteroidDestroyed;
            EventBroker.OnGameReset += OnGameReset;
            OnGameReset();
        }

        private void OnDisable()
        {
            EventBroker.OnAsteroidDestroyed -= OnAsteroidDestroyed;
            EventBroker.OnGameReset -= OnGameReset;
        }

        private void OnAsteroidDestroyed(bool byPlayer)
        {
            if (byPlayer)
            {
                score++;

                if (score >= GameSettings.instance.MaxScore)
                {
                    FinishGame();
                }
            }
            EventBroker.CallOnScoreUpdated(score);
        }

        private void FinishGame()
        {
            Time.timeScale = 0;
            EventBroker.CallOnGameFinished();
        }

        private void OnGameReset()
        {
            score = 0;
            Time.timeScale = 1;
            EventBroker.CallOnScoreUpdated(score);
        }
    }
}