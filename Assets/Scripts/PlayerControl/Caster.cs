using UnityEngine;

namespace AsteroidClicker.PlayerControl
{
    public class Caster
    {
        private Ray ray;
        private RaycastHit hit;
        private Camera castingCamera;

        public Caster()
        {
            castingCamera = Camera.main;
        }

        public bool Cast<T>(Vector3 position, out T result)
        {
            ray = castingCamera.ScreenPointToRay(position);
            if (Physics.Raycast(ray, out hit))
            {
                result = hit.transform.GetComponent<T>();
                if (result != null)
                {
                    return true;
                }
            }
            result = default;
            return false;
        }
    }
}