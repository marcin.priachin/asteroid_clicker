using UnityEngine;

namespace AsteroidClicker.PlayerControl
{
    public class ClicksReader
    {
        private float clickTime;

        public bool GetClick()
        {
            bool click = Input.GetMouseButtonDown(0);
            if (click)
            {
                clickTime = Time.time;
            }
            return click;
        }

        public bool GetDoubleClick()
        {
            float lastClickTime = clickTime;
            if (GetClick())
            {
                bool doubleClick = clickTime - lastClickTime < GameSettings.instance.MaxDoubleClickInterval;
                if (doubleClick)
                {
                    clickTime = -GameSettings.instance.MaxDoubleClickInterval;
                    return true;
                }
            }
            return false;
        }
    }
}