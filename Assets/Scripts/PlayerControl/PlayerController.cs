using AsteroidClicker.Asteroids;
using UnityEngine;

namespace AsteroidClicker.PlayerControl
{
    public class PlayerController : MonoBehaviour
    {
        private ClicksReader clicksReader;
        private Caster caster;

        private void Start()
        {
            caster = new Caster();
            clicksReader = new ClicksReader();
        }

        void Update()
        {
            if (Time.timeScale > 0 && clicksReader.GetDoubleClick())
            {
                if (caster.Cast<Asteroid>(Input.mousePosition, out Asteroid asteroid))
                {
                    asteroid.Hit();
                }
            }
        }
    }
}