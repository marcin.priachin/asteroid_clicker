using UnityEditor;
using UnityEngine;

namespace AsteroidClicker
{
	[CreateAssetMenu(fileName = "GameSettings", menuName = "Custom/Game Settings")]
	public class GameSettings : ScriptableSingleton<GameSettings>
	{
		[Header("Gameplay")]
		[SerializeField] private int maxScore = 15;
		[SerializeField] private float maxDoubleClickInterval = 0.5f;
		[SerializeField] private int minAsteroidsCount = 5;
		[SerializeField] private int maxAsteroidsCount = 10;
		[SerializeField] private float asteroidMinSpawnTime = 1;
		[SerializeField] private float asteroidMaxSpawnTime = 5;
		[Header("Asteroid")]
		[SerializeField] private GameObject asteroidPrefab;
		[SerializeField] private float asteroidMinLifeSpan = 1;
		[SerializeField] private float asteroidMaxLifeSpan = 3;
		[SerializeField] private int asteroidLives = 4;
		[SerializeField] private Color[] asteroidColors;
		[Header("Texts")]
		[SerializeField] private string scorePrefix;
		[SerializeField] private string winText;
		[SerializeField] private string resetText;

		public float MaxScore => maxScore;
		public float MaxDoubleClickInterval => maxDoubleClickInterval;
		public int MinAsteroidsCount => minAsteroidsCount;
		public int MaxAsteroidsCount => maxAsteroidsCount;
		public float AsteroidMinSpawnTime => asteroidMinSpawnTime;
		public float AsteroidMaxSpawnTime => asteroidMaxSpawnTime;
		public float AsteroidRabdomSpawnTime => Random.Range(asteroidMinSpawnTime, asteroidMaxSpawnTime);
		public GameObject AsteroidPrefab => asteroidPrefab;
		public float AsteroidMinLifeSpan => asteroidMinLifeSpan;
		public float AsteroidMaxLifeSpan => asteroidMaxLifeSpan;
		public float AsteroidRandomLifeSpan => Random.Range(asteroidMinLifeSpan, asteroidMaxLifeSpan);
		public int AsteroidLives => asteroidLives;
		public Color[] AsteroidColors => asteroidColors;
		public string ScorePrefix => scorePrefix;
		public string WinText => winText;
		public string ResetText => resetText;
	}
}