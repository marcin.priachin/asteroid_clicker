using UnityEngine;
using UnityEngine.UI;

namespace AsteroidClicker
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private Text scoreText;
        [SerializeField] private GameObject winPanel;
        [SerializeField] private Text winText;
        [SerializeField] private Text resetText;

        private void OnEnable()
        {
            EventBroker.OnScoreUpdated += OnScoreUpdated;
            EventBroker.OnGameFinished += OnGameFinished;
            SetTexts();
        }

        private void OnDisable()
        {
            EventBroker.OnScoreUpdated -= OnScoreUpdated;
            EventBroker.OnGameFinished -= OnGameFinished;
        }

        private void OnScoreUpdated(int score)
        {
            scoreText.text = GameSettings.instance.ScorePrefix + score;
        }

        private void OnGameFinished()
        {
            winPanel.SetActive(true);
        }

        private void SetTexts()
        {
            winText.text = GameSettings.instance.WinText;
            resetText.text = GameSettings.instance.ResetText;
        }

        public void ResetGame()
        {
            winPanel.SetActive(false);
            EventBroker.CallOnGameReset();
        }
    }
}